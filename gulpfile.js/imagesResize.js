/**
 * imagesResize
 * Resize all JPG images to three different sizes: 200, 500, and 630 pixels
 */
const imagesResize = {
  '*.{jpg,png,jpeg,gif,webp}': [{
    crop: true,
    width: 350,
    height: 300,
    suffix: '-thumb'
  }, {
    width: 800,
    suffix: '-full'
  }],
  'slides/*.{jpg,png,jpeg,gif,webp}': [{
    crop: true,
    width: 1200,
    height: 400,
    upscale: true
  }],
  'limpieza-*/*.{jpg,png,jpeg,gif,webp}': [{
    crop: true,
    width: 300,
    height: 350,
    upscale: true,
    suffix: '-thumb'
  }],
  'otros-*/*.{jpg,png,jpeg,gif,webp}': [{
    crop: true,
    width: 300,
    height: 350,
    upscale: true,
    suffix: '-thumb'
  }]
};

module.exports = imagesResize;