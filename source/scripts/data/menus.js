export const menuinicio = [
  {
    title: 'Inicio',
    href: '/',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-users'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact'
  }
]

export const mainmenu = [
  {
    title: 'Limpieza en entidades y empresas',
    href: 'limpieza-en-entidades-y-empresas',
    submenu: [
      {
        title: 'Embajadas y consulados'
      },
      {
        title: 'Entidades públicas'
      },
      {
        title: 'Entidades no Gubernamentales'
      },
      {
        title: 'Entidades financieras'
      }
    ]
  },
  {
    title: 'Limpieza industrial',
    href: 'limpieza-industrial',
    submenu: [
      {
        title: 'Limpieza de fábricas e industrias'
      },
      {
        title: 'Limpieza de almacenes, bodegas'
      },
      {
        title: 'Recojo selectivo de residuos industriales'
      },
      {
        title: 'Limpieza área de máquinas'
      },
      {
        title: 'Limpieza de residuos sólidos urbanos'
      },
      {
        title: 'Limpieza de vidrios en altura'
      },
      {
        title: 'Limpieza de talleres de todo tipo'
      },
      {
        title: 'Limpieza de zonas de circulación'
      },
      {
        title: 'Limpieza de conductos de ventilación y climatización'
      }
    ]
  },
  {
    title: 'Limpieza doméstica',
    href: 'limpieza-domestica',
    submenu: [
      {
        title: 'Limpieza de habitaciones'
      },
      {
        title: 'Lavado de muebles con tapiz'
      },
      {
        title: 'Lavado, abrillantado y pulido de pisos '
      },
      {
        title: 'Limpieza de espejos'
      },
      {
        title: 'Limpieza y desempolvado de fachadas, paredes, techos, cielos falsos'
      },
      {
        title: 'Lavado de cortinas, manteles, estores, persianas'
      },
      {
        title: 'Lavado de colchones'
      },
      {
        title: 'Lavado y desengrasado de cocinas y comedores'
      },
      {
        title: 'Limpieza de gradas  y patios'
      },
      {
        title: 'Barrido de pasillos y bauleras'
      },
      {
        title: 'Virutillado, encerado y lustrado de pisos de madera'
      },
      {
        title: 'Lavado de garajes y zonas de estacionamientos'
      }
    ]
  },
  {
    title: 'Limpieza sanitaria',
    href: 'limpieza-sanitaria',
    submenu: [
      {
        title: 'Hospitales, Clínicas, Centros de salud y Postas sanitarias'
      },
      {
        title: 'Limpieza de quirófanos'
      },
      {
        title: 'Salas de extracción de sangre'
      },
      {
        title: 'Unidades de cuidados intensivos'
      },
      {
        title: 'Laboratorios'
      },
      {
        title: 'Salas de neonatos'
      },
      {
        title: 'Salas de urgencias médicas'
      },
      {
        title: 'Salas de consultas'
      },
      {
        title: 'Salas de habitaciones'
      },
      {
        title: 'Limpieza de oficinas'
      }
    ]
  },
  {
    title: 'Otros ambientes',
    href: 'otros-ambientes',
    submenu: [
      {
        title: 'Limpieza de centros deportivos'
      },
      {
        title: 'Limpieza de aeropuertos'
      },
      {
        title: 'Limpieza de terminales de buses'
      },
      {
        title: 'Limpieza  de hoteles, residenciales, hostales y alojamientos'
      },
      {
        title: 'Limpieza de restaurantes'
      },
      {
        title: 'Unidades educativas y Universidades'
      },
      {
        title: 'Guarderías y centros infantiles'
      },
      {
        title: 'Limpieza en eventos y ferias'
      }
    ]
  },
  {
    title: 'Otros servicios',
    href: 'otros-servicios'
  }
]
