import { TweenLite } from 'gsap'

export const effectsEntry = () => {
  // Espatula
  TweenLite.fromTo('#espatula', 1, {x: 530, y: -45}, {x: 0, y: 0, delay: 2})
  TweenLite.fromTo('#espatula', 1, {x: 0, y: 0}, {x: 530, y: -45, delay: 1})
  TweenLite.fromTo('#oh', 1, {opacity: 0.68}, {opacity: 1, delay: 2})
  TweenLite.fromTo('#oh', 1, {opacity: 0.45}, {opacity: 0.68, delay: 1})
  TweenLite.fromTo('#lines', 1, {opacity: 1, x: 530, y: -45}, {opacity: 1, x: 0, y: 0, delay: 1.96})
  TweenLite.fromTo('#lines', 1, {opacity: 0.3, x: 0, y: 0}, {opacity: 0.1, delay: 1})
  // Brillos
  TweenLite.fromTo('#brillo01', 1.8, {scale: 5, rotation: 180, transformOrigin: 'center'}, {scale: 1.2, rotation: 0, delay: 4.8})
  TweenLite.fromTo('#brillo01', 1, {opacity: 0, scale: 0, rotation: 270}, {opacity: 1, scale: 5, rotation: 180, transformOrigin: 'center', delay: 4})

  TweenLite.fromTo('#brillo02', 2, {scale: 5, rotation: 180, transformOrigin: 'center'}, {scale: 1.2, rotation: 0, delay: 5.2})
  TweenLite.fromTo('#brillo02', 1, {opacity: 0, scale: 0, rotation: 270}, {opacity: 1, scale: 5, rotation: 180, transformOrigin: 'center', delay: 4.4})

  TweenLite.fromTo('#brillo03', 2.2, {scale: 5, rotation: 180, transformOrigin: 'center'}, {scale: 1.2, rotation: 0, delay: 5.6})
  TweenLite.fromTo('#brillo03', 1, {opacity: 0, scale: 0, rotation: 270}, {opacity: 1, scale: 5, rotation: 180, transformOrigin: 'center', delay: 4.8})

  // Entrada de Burbujas
  const buble = document.querySelector('svg #bubles').children
  let delayBuble = 2.3
  let radio
  for (let i = 0; i < buble.length; i++) {
    if (Math.random().toFixed(1) > 0.5) { radio = 17 } else { radio = 10 }
    TweenLite.fromTo(buble[buble.length - i - 1], 1,
      {
        opacity: 0.5,
        strokeWidth: 9.7,
        css: {r: '40'}
      },
      {
        strokeWidth: 1.7,
        opacity: 0.9,
        css: {r: `${radio}`},
        delay: delayBuble + 1
      }
    )
    TweenLite.fromTo(buble[buble.length - i - 1], 1,
      {
        opacity: 0,
        strokeWidth: 0,
        css: {r: '0'}
      },
      {
        strokeWidth: 9.7,
        opacity: 0.5,
        css: {r: '40'},
        delay: delayBuble
      }
    )
    delayBuble = delayBuble + 0.1
  }
}
